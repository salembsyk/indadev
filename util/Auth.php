<?php
/**
 * Created by PhpStorm.
 * User: Boussoukaya
 * Date: 24/03/2018
 * Time: 17:46
 */
class Auth
{

    public static function handleLogin()
    {
        @session_start();
        //$logged = $_SESSION['loggedIn'];
        if (empty($_SESSION["loggedIn"])) {
            session_destroy();
            header('location: http://localhost:8888/client/login');
            exit;
        }
    }
}