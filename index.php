<?php
/**
 * Created by PhpStorm.
 * User: Boussoukaya
 * Date: 24/03/2018
 * Time: 17:42
 */
session_start();
require 'config.php';
require 'util/Auth.php';


spl_autoload_register(function($class) {
    include LIBS . $class . '.php';
});
// Load the Core !
$lib = new Core();
$lib->init();