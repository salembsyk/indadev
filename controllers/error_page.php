<?php
/**
 * Created by PhpStorm.
 * User: Boussoukaya
 * Date: 26/03/2018
 * Time: 13:15
 */
class error_page extends Controller
{
        function __construct()
        {
            parent::__construct();
        }

        function  index()
        {   $this->view->title="404 Page Not Found";
        $this->view->render("header");
            $this->view->render("error_page/index");
            $this->view->render("footer");
        }

}