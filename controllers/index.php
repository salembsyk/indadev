<?php
/**
 * Created by PhpStorm.
 * User: Boussoukaya
 * Date: 24/03/2018
 * Time: 17:47
 */


class index extends Controller {

        function __construct()
        {
            parent::__construct();
        }


 function index ()
 {
     $this->view->title="Home page";
     $this->view->render("header");
     $this->view->render("index/index");
     $this->view->render("footer");

 }

}