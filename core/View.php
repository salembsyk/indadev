<?php
/**
 * Created by PhpStorm.
 * User: Boussoukaya
 * Date: 18/03/2018
 * Time: 17:20
 */

class View {

    function __construct() {

    }

    public function render($name, $noInclude = false)
    {
        require 'Views/' . $name . '.php';
    }

}