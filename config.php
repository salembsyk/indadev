<?php
/**
 * Created by PhpStorm.
 * User: Boussoukaya
 * Date: 24/03/2018
 * Time: 17:43
 */

// Always provide a TRAILING SLASH (/) AFTER A PATH
define('URL', 'http://localhost:8888/indadev/');
define('LIBS', 'core/');

define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_NAME', 'sqm');
define('DB_USER', 'root');
define('DB_PASS', 'root');

// The sitewide hashkey, do not change this because its used for passwords!
// This is for other hash keys... Not sure yet
define('HASH_GENERAL_KEY', 'MixitUp200');

// This is for database passwords only
define('HASH_PASSWORD_KEY', 'catsFLYhigh2000miles');
?>
